/*
 *  jProxyGrabber.java
 *
 *  Copyright (C) 2014-2041 Kirill Erakhtin
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Report bugs or new features to: ojiga@ya.ru
 *
 */
package org.ojiga.jProxyGrabber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.nio.CharBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;

import sun.rmi.runtime.Log;

public class jProxyGrabber {
    private static final String VER = "0.0.1";
    private static final String VERSION =
            "findrepe  version " + VER + " alpha (2014-03-10)\n"
            + "Copyright (C) 2014-2014 by Kirill Erakhtin\n";
    private static final String REPORT_BUGS =
            "Report bugs to <ojiga@ya.ru>\n"
            + "            or <https://code.google.com/p/jproxy-grabber/issues>\n";
    private static final String LICENSE =
            VERSION
            + "\n"
            + "This program is free software: you can redistribute it and/or modify\n"
            + "it under the terms of the GNU General Public License as published by\n"
            + "the Free Software Foundation, either version 3 of the License, or\n"
            + "(at your option) any later version.\n"
            + "\n"
            + "This program is distributed in the hope that it will be useful,\n"
            + "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
            + "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
            + "GNU General Public License for more details.\n"
            + "\n"
            + "You should have received a copy of the GNU General Public License\n"
            + "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
            + "\n"
            + REPORT_BUGS;

    private static final String HELP =
            VERSION
            + "\n"
            + "findrepe comes with ABSOLUTELY NO WARRANTY. This is free software, and you\n"
            + "are welcome to redistribute it under certain conditions. See the GNU\n"
            + "General Public Licence version 3 for details.\n"
            + "\n"
            + "usage: ojigaProxy.jar [options] [filename] {URL]\n"
            + "       java -jar ojigaProxy.jar [options] [filename] [URL]\n"
            + "\n"
            + "Options:\n"
            + " -v, --verbose               increase verbosity\n"
            + " -V, --verbose-logger        format messages as a logger\n"
            + " -L, --license               display software license\n"
            + " -t, --max-timeout=N         proxy's timeout in milliseconds \n"
            + "     --version               print version number\n"
            + "     --examples              print some useful examples\n"
            + "(-h) --help                  show this help (-h works with no other options)\n"
            + "\n"
            + "\n"
            + REPORT_BUGS;
	private static final String VERBOSE = "-verbose";
	private static final String OBJECTIVE = "-obj";
	private static final String MAX_TIMEOUT = "--max-timeout";
	private static final String SORT = "-sort";
	private static final String EXPLAIN = "-explain";
	private static final String ENCODING = "-encoding";

    private static boolean verbose = false;
    private static int encoding;
    private static long arg_max_timeout;
    
    
	private static String fromDate;
	private static String toDate;
	static URL url;
	static URL url4Check;
	static int status;
	static int i;
	static private String rawData;
	static private String rawHREF;
	static private String urlClean;
	static private URLConnection conn;
	static private BufferedReader serverResponse;
	static private String szIPregex;	 
	static private String arURLs[] = {	 "4travel.jp", "advogato.org", "ameba.jp", "anobii.com", "asmallworld.net", "backtype.com", "badoo.com", "bebo.com", "bigadda.com", "bigtent.com", "biip.no", "blackplanet.com", "blog.seesaa.jp", "blogspot.com", "blogster.com", "blomotion.jp", "bolt.com", "brightkite.com", "buzznet.com", "cafemom.com", "care2.com", "classmates.com", "cloob.com", "cyworld.co.kr", "dailymotion.com", "delicious.com", "deviantart.com", "digg.com", "diigo.com", "disqus.com", "draugiem.lv", "facebook.com", "faceparty.com", "fc2.com", "flickr.com", "flixster.com", "fotolog.com", "foursquare.com", "friendfeed.com", "friendsreunited.com", "friendster.com", "fubar.com", "gaiaonline.com", "geni.com", "goodreads.com", "grono.net", "habbo.com", "hatena.ne.jp", "hi5.com", "hotnews.infoseek.co.jp", "hyves.nlibibo.comidenti.ca", "imeem.com", "intensedebate.com", "irc-galleria.net", "iwiw.hu", "jaiku.com", "jp.myspace.com", "kaixin001.com", "kaixin002.com", "kakaku.com", "kanshin.com", "kozocom.com", "last.fm", "linkedin.com", "livejournal.com", "matome.naver.jp", "me2day.net", "meetup.com", "mister-wong.com", "mixi.jp", "mixx.com", "mouthshut.com", "multiply.com", "myheritage.com", "mylife.com", "myspace.com", "myyearbook.com", "nasza-klasa.pl", "netlog.com", "nettby.no", "netvibes.com", "nicovideo.jp", "ning.com", "odnoklassniki.ru", "orkut.com", "pakila.jp", "photobucket.com", "pinterest.com", "plaxo.com", "plurk.com", "plus.google.com", "reddit.com", "renren.com", "skyrock.com", "slideshare.net", "smcb.jp", "smugmug.com", "sonico.com", "studivz.net", "stumbleupon.com", "t.co", "t.hexun.com", "t.ifeng.com", "t.sohu.com", "tabelog.com", "tagged.com", "taringa.net", "thefancy.com", "tripit.com", "trombi.com", "trytrend.jp", "tuenti.com", "tumblr.com", "twine.com", "twitter.com", "uhuru.jp", "viadeo.comvimeo.com", "vk.com", "vox.com", "wayn.com", "weibo.com", "weourfamily.com", "wer-kennt-wen.de", "wordpress.com", "xanga.com", "xing.com", "yammer.com", "yaplog.jp", "yelp.com", "youku.com", "youtube.com", "yozm.daum.net", "yuku.com", "zooomr.com"
 
									};		
	static private int countLiveProxy;
	
	
	public static void main(String[] args) throws IOException {
		try {
			
			String options = processArguments(args);

			Calendar cal = Calendar.getInstance();
			toDate = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
			cal.add(Calendar.WEEK_OF_YEAR, -1);
			fromDate = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
			status = 0;
				
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			
			url = new URL("http://www.google.ru/search?q=3128+8080+" + fromDate + "+proxy" );
				
			conn = url.openConnection();
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
			conn.connect();

			serverResponse = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while( (rawData = serverResponse.readLine()) != null) {
				if (rawData.contains("webcache.googleusercontent.com")){
						rawHREF = rawHREF + rawData;
					}
				}
				
			rawHREF = rawHREF.replaceAll("href.........", "\n");
			rawHREF = rawHREF.replaceAll("%2B", "\n");
			rawHREF = rawHREF.replaceAll("\\&amp\\;", "   ");
			rawHREF = rawHREF.replaceAll("http\\:", "\nhttp\\:");
			rawHREF = rawHREF.replaceAll("\\%3F", "\\?");
			rawHREF = rawHREF.replaceAll("\\%3D", "\\=");
			rawHREF = rawHREF.replaceAll("http\\:", "\nhttp\\:");
				
			ArrayList links = new ArrayList();
				
			String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(rawHREF);
			while(m.find()) {
			urlClean = m.group();
			if (urlClean.startsWith("(") && urlClean.endsWith(")"))
				{
				urlClean = urlClean.substring(1, urlClean.length() - 1);
				}
				if(urlClean.contains("http")){
					if(!urlClean.contains("googleusercontent")){
						if(!urlClean.contains("google")){
							if(!urlClean.contains("schema")){
								if(!urlClean.contains("%")){
									links.add(urlClean);
									downloadPages(urlClean);
									}
								}
							}
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}			

		

	}		 

	public static boolean isProxy(String iPaddress){
		final Pattern IP_PATTERN =
			  Pattern.compile("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{1,5}");
		return IP_PATTERN.matcher(iPaddress).matches();
}
	
	public static void downloadPages( String urlsWithIPs ) throws IOException{
		String rawIPs = null;
		String szStatus;
		url = new URL(urlsWithIPs );
		conn = url.openConnection();
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
		conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

		
		try {
			conn.connect();
		} catch (MalformedURLException e2) {
			//e2.printStackTrace();
			return;
		}

		try {
			serverResponse = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} catch (MalformedURLException e2) {
			//e2.printStackTrace();
			return;
		}
		
		while( (rawData = serverResponse.readLine()) != null) {
			rawIPs = rawIPs + rawData;
			}

		rawIPs = rawIPs.replaceAll("img", "\n");
		rawIPs = rawIPs.replaceAll("\n", "\n\n#separate#");
		rawIPs = rawIPs.replaceAll("\\<\\/td\\>						\\<td\\>", ":");
		rawIPs = rawIPs.replaceAll("\\<br\\>", "#separate#");
		rawIPs = rawIPs.replaceAll("\\<b\\>", "#separate#");
		rawIPs = rawIPs.replaceAll("\\<\\/td\\>\\<td\\>", ":");
		
		
		szIPregex = "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{1,4}";
		Pattern p = Pattern.compile(szIPregex);
		Matcher m = p.matcher(rawIPs);
		while(m.find()) {
			urlClean = m.group();
//			szStatus = CheckSock4Proxy(urlClean);
			if(!urlClean.contains(" ")){
				szStatus = CheckHttpProxy(urlClean);
				if(szStatus != ""){
					WriteResultToFIle(urlClean + " " + szStatus);				
					System.out.println("Status: " + /*urlClean +*/ " " + szStatus);
				}else{
					//System.out.println("Status: " + urlClean + " disabled");
					
				}
			}
		}
		
		
	}

	
	private static String CheckSock4Proxy(String IPwithPort)    {
		String IsSock4Proxy="SOCK4";
		String szPort="";
		String szIP;
		String[] token;
		String delims = "[:]";
		token = IPwithPort.split(delims);
		szIP = token[0];
		szPort = token[1];
        URL url4Check = null;
        String rawContent;
        String szContent = null;

		System.setProperty("socksProxySet", "true");
		System.setProperty("socksProxyHost", szIP);
        System.setProperty("socksProxyPort", szPort);
		
        Random rnd = new Random();
        String szRandomURL = arURLs[rnd.nextInt(141)];

        try {
			url4Check = new URL("http://" + szRandomURL);
		} catch (MalformedURLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

        try {
			conn = url4Check.openConnection();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
		conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		conn.setConnectTimeout(1500);
		conn.setReadTimeout(1500);

		try {
			conn.connect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}

		try {
			serverResponse = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} catch (IOException e) {
//			e.printStackTrace();
		}
		try {
			while( (rawContent = serverResponse.readLine()) != null) {
				szContent = szContent + rawContent;
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		if(szContent != null){
			IsSock4Proxy = "";
		}
		System.setProperty("socksProxySet", "false");

		return IsSock4Proxy;
}
	
	
	private static String CheckHttpProxy(String IPwithPort)    {
		String IsHTTPProxy="";
		String szPort="";
		String szIP;
		String[] token;
		String delims = "[:]";
		token = IPwithPort.split(delims);
		szIP = token[0];
		szPort = token[1];
		URL url4Check = null;
        String rawContent= "";
        String szContent = "";

		System.setProperty("http.proxySet", "true");

		System.setProperty("http.proxyHost", szIP);
		System.setProperty("http.proxyPort", szPort);

        Random rnd = new Random();
        String szRandomURL = arURLs[rnd.nextInt(133)];

		try {
			url4Check = new URL("http://" + szRandomURL);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}

		try {
			conn = url4Check.openConnection();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
		
		
		conn.setConnectTimeout(15000);
		conn.setReadTimeout(15000);

		Calendar lCDateTime = Calendar.getInstance();
		long lMilliseconds =  lCDateTime.getTimeInMillis();

		try {
			conn.connect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//IsHTTPProxy = "";
		}

		try {
			serverResponse = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} catch (IOException e) {
//			IsHTTPProxy = "";
//			e.printStackTrace();
		}

		try {
			while( (rawContent = serverResponse.readLine()) != null) {
					szContent = szContent + rawContent;
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		System.setProperty("http.proxySet", "false");
        System.setProperty("http.proxyHost", "");
        System.setProperty("http.proxyPort", "");

        //if(IPwithPort.contains(" "))
        //	return "";
        

		if(szContent.length() > 1){
			lCDateTime = Calendar.getInstance();
			lMilliseconds = (lCDateTime.getTimeInMillis() - lMilliseconds)/1000;
			//System.out.println(lMilliseconds + "<" + arg_max_timeout);
			if ( lMilliseconds < arg_max_timeout){
				IsHTTPProxy = "HTTP " + IPwithPort + " " + Long.toString(lMilliseconds) + " sec";
			}else{
			IsHTTPProxy = "";
			}
			
			//http://iichan.hk/cgi-bin/captcha1.pl/b/?key=mainpage&dummy=
		} 	
        return IsHTTPProxy;
}
	
	private static void WriteResultToFIle(String szResult)	{
		if(szResult != ""){
			try {
				Calendar cal = Calendar.getInstance();
				toDate = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
	 
				File file = new File("./" + toDate + ".txt");
	 
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.append(szResult + "\n");
				bw.close();
	 
	 
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}	
	}

	public static String processArguments(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase(VERBOSE)) {
				verbose = true;
				continue;
			}

			
			if (args[i].contains(MAX_TIMEOUT)) {
				String[] token;
				String delims = "[=]";
				token = args[i].split(delims);
		
				arg_max_timeout = Integer.parseInt(token[1]);
				continue;
			}
			if (args[i].equalsIgnoreCase(EXPLAIN)) {
//				result.explain = true;
				continue;
			}
		}
	 return "Done";
	}
	 
	public static int tryParseInt(String[] str, int orderNumber) {
		  try {
		    return Integer.parseInt(str[orderNumber]);
		  } catch (NumberFormatException e) {
		    return -1;
		  }
		}
}
